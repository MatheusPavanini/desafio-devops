# iti Platform Challenge

## Introdução
Essa documentação tem a finalidade de apresentar todos os passos propostos no iti Platform Challenge.
Serão apresentados tanto os passos manuais, como também a forma automatizada de realizar as tarefas. É importante lembrar que algumas tarefas apresentam dependências entre si.

## Tópicos Apresentados
* Parte1
    * Containerize essa aplicação
    * Crie um Helm chart contendo todos os componentes necessários para essa aplicação rodar em um cluster de Kubernetes
    * Crie uma pipeline com GitLab CI para esse chart ser aplicado em um cluster de Kubernetes
* Parte 2
    * Provisione uma infraestrutura na AWS com terraform para subir essa aplicação containerizada
    * Lembre-se de utilizar EC2, ELB, ASG entre outros serviços da AWS
    * Crie um pipeline com GitLab CI para automatizar a execução destes passos e subir essa infraestrutura na AWS
* Configurações necessárias
    * Configurar acesso da AWS no Gitlab
    * Configurar apontamento do Helmchart


## Containerize essa aplicação
Para realizar a containerização da aplicação, realizei a criação do Dockerfile que apresenta a forma do build esperado bem como seu entrypoint

A partir da pasta raíz do projeto, execute os seguinte comandos para executar a criação, identificação, e publicação do container na AWS:

* Criar imagem: docker build -t <repositório>/<tag-da-imagem> -f Parte1/Dockerfile .
* Criar container local (opcional): docker run -d --name <nome do container> -p 8080:8080 <repositorio>/<aplicação>
* Identificar a imagem: docker tag <repositório>/<aplicação> <endereço do ECR na AWS>/<nome do repositório>:<tag-da-imagem>
* Realizar login na AWS:aws ecr get-login-password --region <região da aws> | docker login --username AWS --password-stdin <endereço do ECR na AWS>
* Enviar imagem para o repositório da AWS: docker push <endereço do ECR na AWS>/<nome do repositório>:<tag-da-imagem>

Observação: Esse passo depende da criação da infraestrutura no passo "Provisione uma infraestrutura na AWS com terraform para subir essa aplicação containerizada"

## Crie um Helm chart contendo todos os componentes necessários para essa aplicação rodar em um cluster de Kubernetes
O helmchart "devopshelmchart" realiza deploy de dois componentes principais: O Ingress e a aplicação kotlin-api.Vale ressaltar que além de utilizar meu helmchart, também utilizei o helmchart do ingress-nginx que será utilizado como ingress controller:

Para realizar o deploy de ambos os componentes execute os comandos abaixos:


* Exportar variavel do kubernetes para se conectar ao k8s: export KUBECONFIG=<arquivo-de-segurança-do-k8s.yml>
* Criar o namespace para o ingress controller: kubectl create namespace ingress-nginx
* Criar o namespace para a api do kotlin: kubectl create namespace kotlin-api

* Adicionar o repo oficial do ingress-nginx: helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
* Realizar a instalação do ingress controller: helm install -n ingress-nginx nginx-devops ingress-nginx/ingress-nginx
* Realizar a instalação do ingress e do kotlin-api: helm install -n kotlin-api kotlin-api1 Parte1/devopschart

Observação: Esse passo depende da criação da infraestrutura no passo "Provisione uma infraestrutura na AWS com terraform para subir essa aplicação containerizada"

## Crie uma pipeline com GitLab CI para esse chart ser aplicado em um cluster de Kubernetes
* A pipeline foi criada como o arquivo .gitlab-ci.yml dentro da pasta Parte1. Para utilizar essa pipeline, mova o arquivo para o diretório raíz e realize um commit em um repositório remoto do gitlab

Obs: Para utilizar a pipeline realize configuração das variáveis de ambiente da AWS apresentadas no passo "Configurar acesso da AWS no Gitlab"

Obs: Esse passo depende da criação da infraestrutura no passo "Provisione uma infraestrutura na AWS com terraform para subir essa aplicação containerizada"

## Provisione uma infraestrutura na AWS com terraform para subir essa aplicação containerizada
Para realizar o aprovisionamento da infraestrutura na AWS partir da sua máquina local, vá até o diretório "Parte2" e execute os seguintes comandos:

* Inciar e baixar os módulos do terraform: terraform init
* Realizar o aprovisionamento de toda infraestrutura: terraform apply


## Lembre-se de utilizar EC2, ELB, ASG entre outros serviços da AWS
Os recursos utilizados na AWS foram aprovisionados no passo anterior. Foram utilizados: EC2, ELB, ASGs, IAM Roles, Security Groups, EKS, ECR, VPC, Subnets, NAT Gateways, Elastic IP

## Crie um pipeline com GitLab CI para automatizar a execução destes passos e subir essa infraestrutura na AWS
A pipeline foi criada como o arquivo .gitlab-ci.yml dentro da diretório "Parte2". Para utilizar essa pipeline, mova o arquivo para o diretório raíz e realize um commit em um repositório remoto do gitlab

No Gitlab, após o job de "plan" finalizar, inicie manualmente o job "deploy"

## Configurar acesso da AWS no Gitlab
Abra sua conta na AWS e va no em configurações -> CI/CD -> Variables e adicione AWS_SECRET_ACCESS_KEY=<id-do-usuário-da-aws> AWS_SECRET_ACCESS_KEY=<chave-secreta-do-usuário-da-aws>

## Configurar apontamento do Helmchart
Para realizar o deploy do helmchart, abra o arquivo /Parte1/devopschart/values.yaml e altere o atributo "repository:" <endereço do ECR na AWS>
