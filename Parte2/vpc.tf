provider "aws" {
  region  = "sa-east-1"
}

module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "3.2.0"

  name = "vpc-eks"
  cidr = "10.0.0.0/16"

  azs             = ["sa-east-1a","sa-east-1b"]
  private_subnets = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]
  public_subnets  = ["10.0.101.0/24", "10.0.102.0/24", "10.0.103.0/24"]
  
  #Verificar
  enable_nat_gateway = true
  enable_vpn_gateway = true
  enable_dns_hostnames = true
  single_nat_gateway = true

  tags = {
    "Name" = "terraform-eks-demo-node"
    "kubernetes.io/cluster/eks-cluster" = "shared"
  }
}
